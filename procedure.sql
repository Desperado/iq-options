CREATE extension IF NOT EXISTS "uuid-ossp";

CREATE OR REPLACE FUNCTION packOperationDataToJSONB() RETURNS TRIGGER AS
$$
DECLARE
    newId varchar(36);
BEGIN
  newId = NEW.id;
  INSERT INTO events (id, data)
  SELECT  uuid_generate_v4() AS id, array_to_json(array_agg(ops.*)) AS data FROM operations ops WHERE id = NEW.id;

  RETURN NEW;
END
$$ LANGUAGE plpgsql;

CREATE TRIGGER packOperationsDetails
AFTER INSERT ON operations FOR EACH ROW EXECUTE PROCEDURE packOperationDataToJSONB();