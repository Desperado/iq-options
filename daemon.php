<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

use App\Daemon\Bootstrap;
use App\Daemon\AmphpDaemon;
use App\Task;

include __DIR__ . '/vendor/autoload.php';

try
{
    $bootstrap = new Bootstrap('events', __DIR__);
    $daemonBackend = new AmphpDaemon($bootstrap->getLogger());

    $eventListenerTask = new Task\EventListenerTask(
        $bootstrap->getQueueBackend(),
        $bootstrap->getPostgresConnection(),
        $bootstrap->getLogger()
    );

    $archiveListenerTask = new Task\ArchiveListenerTask(
        $bootstrap->getQueueBackend(),
        $bootstrap->getPostgresConnection(),
        $bootstrap->getLogger()
    );

    $daemonBackend->run($archiveListenerTask, $eventListenerTask);
}
catch (\Throwable $throwable)
{
    exit(\sprintf('%s (%s:%d)', $throwable->getMessage(), $throwable->getFile(), $throwable->getLine()));
}