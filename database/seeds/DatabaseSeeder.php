<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Stored procedure fixtures
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Load stored procedure fixtures
     *
     * @return void
     */
    public function run(): void
    {
        DB::statement('CREATE extension IF NOT EXISTS "uuid-ossp";');
        DB::statement('CREATE OR REPLACE FUNCTION pack_operation_data_to_jsonb() RETURNS TRIGGER AS
$$
BEGIN
  INSERT INTO events (id, data)
  SELECT  uuid_generate_v4() AS id, row_to_json(ops.*) AS data FROM operations ops WHERE id = NEW.id;

  RETURN NEW;
END
$$ LANGUAGE plpgsql;');

        DB::statement('CREATE TRIGGER pack_operations_details_trigger
AFTER INSERT ON operations FOR EACH ROW EXECUTE PROCEDURE pack_operation_data_to_jsonb();');

        DB::statement('CREATE OR REPLACE FUNCTION notify_backend_daemon() RETURNS trigger AS $$
BEGIN
  PERFORM pg_notify(\'event_watchers\', TG_TABLE_NAME || \',\' || NEW.id );
  RETURN new;
END;
$$ LANGUAGE plpgsql;');

        DB::statement('CREATE TRIGGER watch_event_added_trigger 
        AFTER INSERT ON events FOR EACH ROW EXECUTE PROCEDURE notify_backend_daemon();');
    }
}
