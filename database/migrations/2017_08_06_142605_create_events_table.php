<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database;

/**
 * Create `events` table
 */
class CreateEventsTable extends Database\Migrations\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('events', function (Database\Schema\Blueprint $table)
        {
            $table->uuid('id');
            $table->jsonb('data');
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::drop('events');
    }
}
