<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database;

/**
 * Create `archive` table
 */
class CreateArchiveTable extends Database\Migrations\Migration
{
    /**
     * Run the migrations
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('archive', function (Database\Schema\Blueprint $table)
        {
            $table->uuid('operation_id');
            $table->decimal('sum');
            $table->dateTimeTz('created');
            $table->dateTimeTz('archived');
            $table->primary('operation_id');
            $table
                ->foreign('operation_id')
                ->references('id')
                ->on('operations')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down(): void
    {
        Schema::drop('archive');
    }
}
