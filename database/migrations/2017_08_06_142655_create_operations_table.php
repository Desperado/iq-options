<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database;

/**
 * Create `operations` table
 */
class CreateOperationsTable extends Database\Migrations\Migration
{
    /**
     * Run the migrations
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('operations', function (Database\Schema\Blueprint $table)
        {
            $table->uuid('id');
            $table->decimal('sum', 11, 2);
            $table->dateTimeTz('created');
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down(): void
    {
        Schema::drop('operations');
    }
}
