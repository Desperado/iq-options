<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

namespace App\Queue;

use Amp\Promise;
use Amp\Redis;

/**
 * Non-blocking redis client
 */
class RedisBackend implements QueueInterface
{
    /**
     * Redis client
     *
     * @var Redis\Client
     */
    private $client;

    /**
     * Redis subscriber client
     *
     * @var Redis\SubscribeClient
     */
    private $subscriber;

    /**
     * @param string $connectionDSN
     */
    public function __construct(string $connectionDSN)
    {
        $this->client = new Redis\Client($connectionDSN);
        $this->subscriber = new Redis\SubscribeClient($connectionDSN);
    }

    /**
     * @inheritdoc
     */
    public function send(string $destination, Message $message): void
    {
        $this->client->publish($destination, \serialize($message));
    }

    /**
     * @inheritdoc
     */
    public function subscribe(string $destination, array $headers = []): Promise
    {
        return $this->subscriber->subscribe($destination);
    }

    /**
     * @inheritdoc
     */
    public function unsubscribe(string $subscriptionId, array $headers = [])
    {
        $this->subscriber->close();
    }

    /**
     * @inheritdoc
     */
    public function ack(string $messageId, string $transactionId = null, array $headers = []): void
    {

    }

    /**
     * @inheritdoc
     */
    public function nack(string $messageId, string $transactionId = null, array $headers = []): void
    {

    }
}
