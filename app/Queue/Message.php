<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

namespace App\Queue;

/**
 * Message DTO
 */
class Message implements \Serializable
{
    /**
     * Message ID
     *
     * @var string
     */
    private $id;

    /**
     * Message DTO
     *
     * @var \Serializable
     */
    private $payload;

    /**
     * Message headers
     *
     * @var array
     */
    private $headers = [];

    /**
     * @param string        $id
     * @param \Serializable $dtoObject
     * @param array         $headers
     */
    public function __construct($id, \Serializable $dtoObject, array $headers = [])
    {
        $this->id = $id;
        $this->payload = $dtoObject;
        $this->headers = $headers;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize(): string
    {
        return \serialize(
            [
                $this->id,
                $this->payload,
                $this->headers
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized): void
    {
        list($this->id, $this->payload, $this->headers) = \unserialize($serialized);
    }

    /**
     * Get message ID
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Get payload DTO
     *
     * @return \Serializable
     */
    public function getPayload(): \Serializable
    {
        return $this->payload;
    }

    /**
     * Get headers
     *
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }
}
