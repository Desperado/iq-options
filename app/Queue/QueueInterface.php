<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

namespace App\Queue;

use Amp\Promise;

/**
 * Queue backend interface
 */
interface QueueInterface
{
    /**
     * Subscribe to queue
     *
     * @param string $destination
     * @param array  $headers
     *
     * @return Promise
     */
    public function subscribe(string $destination, array $headers = []): Promise;

    /**
     * Unsubscribe from queue
     *
     * @param string $subscriptionId
     * @param array  $headers
     *
     * @return mixed
     */
    public function unsubscribe(string $subscriptionId, array $headers = []);

    /**
     * Send message to queue
     *
     * @param string  $destination
     * @param Message $message
     *
     * @return void
     */
    public function send(string $destination, Message $message): void;

    /**
     * To take the message in processing
     *
     * @param string      $messageId
     * @param string|null $transactionId
     * @param array       $headers
     *
     * @return void
     */
    public function ack(string $messageId, string $transactionId = null, array $headers = []): void;

    /**
     * Not acknowledge consumption of a message from a subscription
     *
     * @param string      $messageId
     * @param string|null $transactionId
     * @param array       $headers
     *
     * @return void
     */
    public function nack(string $messageId, string $transactionId = null, array $headers = []): void;
}
