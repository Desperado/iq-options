<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

namespace App\Daemon;

use Amp\Deferred;
use App\Extensions\Monolog\Handlers\ConsoleOutputHandler;
use App\Queue\QueueInterface;
use App\Queue\RedisBackend;
use Dotenv\Dotenv;
use Amp\Postgres;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Monolog\Processor;

/**
 * Daemons bootstrap
 */
class Bootstrap
{
    /**
     * Application root directory path
     *
     * @var string
     */
    private $rootDirectoryPath;

    /**
     * Postgres deferred connection
     *
     * @var Postgres\ConnectionPool
     */
    private $postgresConnection;

    /**
     * Queue backend
     *
     * @var QueueInterface
     */
    private $queueBackend;

    /**
     * Logger instance
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Entry point name
     *
     * @var string
     */
    private $entryPointName;

    /**
     * @param string               $entryPointName
     * @param string               $rootDirectoryPath
     * @param LoggerInterface|null $logger
     */
    public function __construct(string $entryPointName, string $rootDirectoryPath, LoggerInterface $logger = null)
    {
        $this->entryPointName = $entryPointName;
        $this->rootDirectoryPath = $rootDirectoryPath;
        $this->logger = $logger ?: $this->getDefaultLogger();

        $this->init();
    }

    /**
     * Get logger instance
     *
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    /**
     * Get deferred postgres connection
     *
     * @return Postgres\ConnectionPool
     */
    public function getPostgresConnection(): Postgres\ConnectionPool
    {
        return $this->postgresConnection;
    }

    /**
     * Get queue backend
     *
     * @return QueueInterface
     */
    public function getQueueBackend(): QueueInterface
    {
        return $this->queueBackend;
    }

    /**
     * Init components
     *
     * @return void
     */
    protected function init(): void
    {
        $this->initDotenv();
        $this->initPostgresConnection();
        $this->initQueueBackend();
    }

    /**
     * Init default logger instance
     *
     * @return LoggerInterface
     */
    protected function getDefaultLogger(): LoggerInterface
    {
        return new Logger(
            $this->entryPointName,
            [
                new ConsoleOutputHandler()
            ],
            [
                new Processor\ProcessIdProcessor(),
                new Processor\PsrLogMessageProcessor()
            ]
        );
    }

    /**
     * Init queue backend
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    private function initQueueBackend()
    {
        $dsnParts = \parse_url((string) \getenv('QUEUE_DSN'));

        if (false === empty($dsnParts['scheme']) && false === empty($dsnParts['path']))
        {
            switch ($dsnParts['scheme'])
            {
                case 'redis':
                    $this->queueBackend = new RedisBackend($dsnParts['path']);
                    break;

                default:

                    throw new \InvalidArgumentException(
                        'The specified queue adapter is not supported (variable "QUEUE_DSN")'
                    );
            }

            $this->logger->debug(
                'The connection to the queue server ("{queueBackend}" adapter) was successfully initiated',
                ['queueBackend' => \get_class($this->queueBackend)]
            );
        }
        else
        {
            throw new \InvalidArgumentException('The value of the environment variable "QUEUE_DSN" is incorrect.');
        }
    }

    /**
     * Init postgres deferred connection
     *
     * @return void
     */
    private function initPostgresConnection(): void
    {
        $this->postgresConnection = Postgres\pool(
            \sprintf(
                'host=%s port=%d user=%s password=%s dbname=%s options=\'--client_encoding=UTF8\'',
                \getenv('DB_HOST'),
                \getenv('DB_PORT'),
                \getenv('DB_USERNAME'),
                \getenv('DB_PASSWORD'),
                \getenv('DB_DATABASE')
            )
        );

        $this->logger->debug('Connection to postgres database successfully initiated');
    }

    /**
     * Init Dotenv
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    private function initDotenv(): void
    {
        if (true === \file_exists($this->rootDirectoryPath) && true === \is_readable($this->rootDirectoryPath))
        {
            (new Dotenv($this->rootDirectoryPath))->load();

            $this->logger->debug('The Dotenv library successful initiated');
        }
        else
        {
            throw new \InvalidArgumentException('.env file not found or unreadable');
        }
    }
}
