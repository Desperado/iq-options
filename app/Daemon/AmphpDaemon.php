<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

namespace App\Daemon;

use Amp\Loop;
use App\Daemon\Interfaces\DaemonBackendInterface;
use App\Task\AbstractTask;
use Psr\Log\LoggerInterface;

/**
 * AMPHP daemon implementation
 */
class AmphpDaemon implements DaemonBackendInterface
{
    /**
     * Logger instance
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;

        $this->initSignals();
        $this->initErrorHandler();
    }

    /**
     * @inheritdoc
     */
    public function run(AbstractTask ...$taskCollection): void
    {
        Loop::run(function () use ($taskCollection)
        {
            $this->logger->debug('AMPHP daemon started');

            foreach ($taskCollection as $task)
            {
                Loop::repeat($task->getTickDelayInterval(), $task);
            }
        });
    }

    /**
     * @inheritdoc
     */
    public function stop(): void
    {
        Loop::stop();
    }

    /**
     * Init error handler
     *
     * @return void
     */
    private function initErrorHandler(): void
    {
        Loop::setErrorHandler(function (\Throwable $throwable)
        {

        });
    }

    /**
     * Init signal handler
     *
     * @return void
     */
    private function initSignals(): void
    {
        Loop::onSignal(SIGINT, function ()
        {
            $this->logger->debug('AMPHP daemon stopped');
            exit(0);
        });
    }
}
