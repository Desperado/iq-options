<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

namespace App\Daemon\Interfaces;

use App\Task\AbstractTask;

/**
 * Daemons backend interface
 */
interface DaemonBackendInterface
{
    /**
     * Run backend
     *
     * @param AbstractTask[] ...$taskCollection
     *
     * @return void
     */
    public function run(AbstractTask ...$taskCollection): void;

    /**
     * Stop backend
     *
     * @return void
     */
    public function stop(): void;
}
