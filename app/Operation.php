<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Operations model
 */
class Operation extends Model
{
    /** For external usage */
    public const TABLE_NAME = 'operations';

    /**
     * @inheritdoc
     */
    public function __construct(array $attributes = [])
    {
        $this->table = self::TABLE_NAME;
        $this->timestamps = false;
        $this->fillable = ['id', 'sum', 'created'];

        parent::__construct($attributes);
    }
}
