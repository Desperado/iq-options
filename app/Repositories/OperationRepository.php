<?php

/**
 * Non-blocking concurrency framework (CQRS, EventSourcing)
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

namespace App\Repositories;

use App\Operation;
use Ramsey\Uuid\Uuid;

/**
 * Operations repository
 */
class OperationRepository
{
    /**
     * Store operation parameters
     *
     * @param array $parameters
     *
     * @return Operation
     */
    public function store(array $parameters): Operation
    {
        $operation = new Operation();

        $parameters = \array_merge($parameters, [
            'id'      => Uuid::uuid4()->toString(),
            'created' => \date('c')
        ]);

        $operation
            ->fill($parameters)
            ->save();

        return $operation;
    }
}
