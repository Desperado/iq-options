<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Events model
 */
class Event extends Model
{
    /** For external usage */
    public const TABLE_NAME = 'events';

    /**
     * @inheritdoc
     */
    public function __construct(array $attributes = [])
    {
        $this->table = self::TABLE_NAME;
        $this->timestamps = false;

        parent::__construct($attributes);
    }
}
