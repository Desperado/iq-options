<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Archive model
 */
class Archive extends Model
{
    /** For external usage */
    public const TABLE_NAME = 'archive';

    /**
     * @inheritdoc
     */
    public function __construct(array $attributes = [])
    {
        $this->table = self::TABLE_NAME;
        $this->timestamps = false;

        parent::__construct($attributes);
    }
}
