<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Operation;
use App\Repositories\OperationRepository;
use Illuminate\Http;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;

/**
 * Operations controller
 */
class OperationController extends Controller
{
    /**
     * Store operation
     *
     * @param string $number
     *
     * @return Http\Response
     */
    public function store(string $number): Http\Response
    {
        try
        {
            if (0 >= (float) $number)
            {
                throw new \InvalidArgumentException('The number must be greater than zero');
            }

            $repository = new OperationRepository(new Operation());
            $repository->store(['sum' => $number]);

            return new Http\Response('Saved successfully', Http\Response::HTTP_ACCEPTED);
        }
        catch (\InvalidArgumentException $exception)
        {
            return new Http\Response($exception->getMessage(), Http\Response::HTTP_BAD_REQUEST);
        }
        catch (\Throwable $throwable)
        {

            Log::info(
                \sprintf(
                    'The exception "%s (%s:%d)" was intercepted while saving the operation',
                    $throwable->getMessage(),
                    $throwable->getFile(),
                    $throwable->getFile()
                )
            );

            return new Http\Response('Internal error', Http\Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
