<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

namespace App\Extensions\Monolog\Handlers;

use Monolog\Handler\AbstractProcessingHandler;

/**
 * Simple console handler (echo handler)
 */
class ConsoleOutputHandler extends AbstractProcessingHandler
{
    /**
     * {@inheritdoc}
     */
    public function close(): void
    {
        parent::close();
    }

    /**
     * {@inheritdoc}
     */
    protected function write(array $record): void
    {
        if ('cli' === \PHP_SAPI)
        {
            echo (string) $record['formatted'];
        }
    }
}
