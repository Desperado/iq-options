<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

namespace App\Utils;

/**
 * json_encode/json_decode wrapper
 */
class JsonSerializerHelper
{
    /**
     * json_encode
     *
     * @param mixed $value
     * @param int   $options
     * @param int   $depth
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    public static function toJson($value, int $options = 0, int $depth = 512): string
    {
        \json_last_error();

        $json = \json_encode($value, $options, $depth);

        if (JSON_ERROR_NONE === \json_last_error())
        {
            return $json;
        }

        throw new \RuntimeException(\json_last_error_msg());
    }

    /**
     * json_decode
     *
     * @param string $json
     * @param bool   $assoc
     * @param int    $depth
     * @param int    $options
     *
     * @return array|\stdClass
     *
     * @throws \RuntimeException
     */
    public static function fromJson(string $json, bool $assoc = false, int $depth = 512, int $options = 0)
    {
        \json_last_error();

        $data = \json_decode($json, $assoc, $depth, $options);

        if (JSON_ERROR_NONE === \json_last_error())
        {
            return $data;
        }

        throw new \RuntimeException(\json_last_error_msg());
    }

    /**
     * Close constructor
     */
    private function __construct()
    {

    }
}
