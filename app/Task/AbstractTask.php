<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

namespace App\Task;

use Amp\Deferred;
use App\Queue\QueueInterface;
use Psr\Log\LoggerInterface;
use Amp\Postgres\ConnectionPool;

/**
 * Base task class
 */
abstract class AbstractTask
{
    /**
     * Queue backend interface
     *
     * @var QueueInterface
     */
    private $queueBackend;

    /**
     * Postgres deferred connection
     *
     * @var ConnectionPool
     */
    private $postgresConnection;

    /**
     * Logger instance
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param QueueInterface  $queueBackend
     * @param ConnectionPool  $postgresConnection
     * @param LoggerInterface $logger
     */
    public function __construct(QueueInterface $queueBackend, ConnectionPool $postgresConnection, LoggerInterface $logger)
    {
        $this->queueBackend = $queueBackend;
        $this->postgresConnection = $postgresConnection;
        $this->logger = $logger;
    }

    /**
     * Get delay interval (Event loop tick)
     *
     * @return int
     */
    abstract public function getTickDelayInterval(): int;

    /**
     * Execute task
     *
     * @return mixed
     */
    abstract public function __invoke();

    /**
     * Get queue backend instance
     *
     * @return QueueInterface
     */
    protected function getQueueBackend(): QueueInterface
    {
        return $this->queueBackend;
    }

    /**
     * Get postgres deferred connection
     *
     * @return ConnectionPool
     */
    protected function getPostgresConnection(): ConnectionPool
    {
        return $this->postgresConnection;
    }

    /**
     * Get logger instance
     *
     * @return LoggerInterface
     */
    protected function getLogger(): LoggerInterface
    {
        return $this->logger;
    }
}
