<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

namespace App\Task;

use App\DTO\EventDTO;
use App\Event;
use App\Queue\Message;
use App\Utils\JsonSerializerHelper;
use Ramsey\Uuid\Uuid;

/**
 * Reading the events table
 */
class EventListenerTask extends AbstractTask
{
    private const LISTEN_CHANNEL = 'event_watchers';

    /**
     * @inheritdoc
     */
    public function getTickDelayInterval(): int
    {
        return 300;
    }

    /**
     * @inheritdoc
     */
    public function __invoke()
    {
        /** @var \Amp\Postgres\Connection $connection */
        $connection = $this->getPostgresConnection();

        /** @var \Amp\Postgres\Listener $listener */
        $listener = yield $connection->listen(self::LISTEN_CHANNEL);

        while (yield $listener->advance())
        {
            $notification = $listener->getCurrent();
            $payloadParts = \explode(',', $notification->payload);

            /** Listen only `events` changes */
            if (false === empty($payloadParts) && Event::TABLE_NAME === $payloadParts[0])
            {
                $this
                    ->getLogger()
                    ->debug(
                        \sprintf(
                            'A notification has been received about a new entry (ID "%s") in the %s table.',
                            $payloadParts[1], Event::TABLE_NAME
                        )
                    );

                /** @var \Amp\Postgres\Statement $selectStatement */
                $selectStatement = yield $connection
                    ->prepare(
                        \sprintf('SELECT id, data FROM %s WHERE id = $1', Event::TABLE_NAME)
                    );

                /** @var \Amp\Postgres\TupleResult $selectResult */
                $selectResult = yield $selectStatement->execute($payloadParts[1]);

                while (yield $selectResult->advance())
                {
                    $eventRow = $selectResult->getCurrent();

                    $message = self::createMessageFromEventRow($eventRow);

                    $this
                        ->getQueueBackend()
                        ->send(
                            ArchiveListenerTask::QUEUE_DESTINATION,
                            $message
                        );

                    $this
                        ->getLogger()
                        ->debug(
                            \sprintf(
                                'Event with id "%s" successfully added to the queue ("%s" destination)',
                                $eventRow['id'], ArchiveListenerTask::QUEUE_DESTINATION
                            )
                        );
                }

                unset($selectStatement, $selectResult);
            }
        }
    }

    /**
     * Transform event row to message instance
     *
     * @param array $rowData
     *
     * @return Message
     */
    private static function createMessageFromEventRow(array $rowData): Message
    {
        $operationData = array_map('strval', JsonSerializerHelper::fromJson($rowData['data'], true));

        $eventDto = new EventDTO(
            (string) $rowData['id'],
            (string) $operationData['id'],
            (string) $operationData['sum'],
            new \DateTimeImmutable($operationData['created'])
        );

        return new Message(Uuid::uuid4()->toString(), $eventDto);
    }
}
