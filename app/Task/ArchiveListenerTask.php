<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

namespace App\Task;

use Amp\Loop;
use App\Archive;
use App\DTO\EventDTO;
use App\Queue\Message;

/**
 * Reads queue and writes data to the archive table
 */
class ArchiveListenerTask extends AbstractTask
{
    public const QUEUE_DESTINATION = 'archive';

    /**
     * @inheritdoc
     */
    public function getTickDelayInterval(): int
    {
        return 200;
    }

    /**
     * @inheritdoc
     */
    public function __invoke()
    {
        Loop::run(
            function ()
            {
                $backend = $this->getQueueBackend();

                /** @var \Amp\Redis\Subscription $subscription */
                $subscription = yield $backend->subscribe(self::QUEUE_DESTINATION);
                $subscription
                    ->advance()
                    ->onResolve(
                        function (\Throwable $throwable = null) use ($subscription)
                        {
                            if (null !== $throwable)
                            {
                                $this
                                    ->getLogger()
                                    ->error(
                                        \sprintf('Subscription start failed with error "%s"', $throwable->getMessage())
                                    );

                                return;
                            }

                            /** @var Message $message */
                            $message = \unserialize($subscription->getCurrent());
                            $payload = $message->getPayload();

                            if (true === ($payload instanceof EventDTO))
                            {
                                /** @var EventDTO $payload */
                                $payload = $message->getPayload();

                                /** @var \Amp\Postgres\Connection $connection */
                                $connection = $this->getPostgresConnection();

                                /** @var \Amp\Postgres\Statement $statement */
                                $statement = yield $connection->prepare(
                                    \sprintf('INSERT INTO %s VALUES ($1, $2, $3, $4)', Archive::TABLE_NAME)
                                );

                                $insertPromise = $statement->execute(
                                    $payload->getOperationId(),
                                    $payload->getOperationSum(),
                                    $payload->getOperationDatetime()->format('c'),
                                    \date('c')
                                );

                                $insertPromise
                                    ->onResolve(
                                        function (\Throwable $throwable = null, $result) use ($message)
                                        {
                                            if (null !== $throwable)
                                            {
                                                return $this->failedStore($message);
                                            }

                                            $this->successStore($message);
                                        }
                                    );

                                unset($statement);
                            }
                        }
                    );
            }
        );
    }

    /**
     * Handles failed store event operation
     *
     * @param Message $message
     *
     * @return self
     */
    private function failedStore(Message $message): self
    {
        /** @var EventDTO $payload */
        $payload = $message->getPayload();

        $this
            ->getLogger()
            ->error(
                \sprintf(
                    'An exception was thrown when adding an operation with ID "%s" to the archive. Try retrying',
                    $payload->getOperationId()
                )
            );

        Loop::delay(3000,
            function () use ($message)
            {
                $this
                    ->getQueueBackend()
                    ->send(self::QUEUE_DESTINATION, $message);
            }
        );

        return $this;
    }

    /**
     * Handles successful store event operation
     *
     * @param Message $message
     *
     * @return self
     */
    private function successStore(Message $message): self
    {
        /** @var EventDTO $payload */
        $payload = $message->getPayload();

        $this
            ->getLogger()
            ->debug(
                \sprintf(
                    'Event with ID "%s" (for operation with ID "%s") successful stored into %s table',
                    $payload->getEventId(), $payload->getOperationId(), Archive::TABLE_NAME
                )
            );

        return $this;
    }
}
