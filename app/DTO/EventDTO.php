<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

namespace App\DTO;

/**
 * Event Data Transfer Object
 */
class EventDTO implements \Serializable
{
    /**
     * Event ID
     *
     * @var string
     */
    private $eventId;

    /**
     * Operation ID
     *
     * @var string
     */
    private $operationId;

    /**
     * Operation sum
     *
     * @var string
     */
    private $operationSum;

    /**
     * Operation datetime
     *
     * @var \DateTimeImmutable
     */
    private $operationDatetime;

    /**
     * @param string             $eventId
     * @param string             $operationId
     * @param string             $operationSum
     * @param \DateTimeImmutable $operationDatetime
     */
    public function __construct(string $eventId, string $operationId, string $operationSum, \DateTimeImmutable $operationDatetime)
    {
        $this->eventId = $eventId;
        $this->operationId = $operationId;
        $this->operationSum = $operationSum;
        $this->operationDatetime = $operationDatetime;
    }

    /**
     * @inheritdoc
     */
    public function serialize(): string
    {
        return \serialize(
            [
                $this->eventId,
                $this->operationId,
                $this->operationSum,
                $this->operationDatetime->format('c')
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function unserialize($serialized): void
    {
        $data = \unserialize($serialized);
        [$this->eventId, $this->operationId, $this->operationSum] = $data;

        $this->operationDatetime = new \DateTimeImmutable($data[3]);
    }

    /**
     * Get event ID
     *
     * @return string
     */
    public function getEventId(): string
    {
        return $this->eventId;
    }

    /**
     * Get operation ID
     *
     * @return string
     */
    public function getOperationId(): string
    {
        return $this->operationId;
    }

    /**
     * Get operation sum
     *
     * @return string
     */
    public function getOperationSum(): string
    {
        return $this->operationSum;
    }

    /**
     * Get operation datetime
     *
     * @return \DateTimeImmutable
     */
    public function getOperationDatetime(): \DateTimeImmutable
    {
        return $this->operationDatetime;
    }
}
