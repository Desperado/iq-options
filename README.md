## Тестовое задание:
```bash
Фреймворк Laravel.
Сделать миграцию, создающую таблицу operations с полями id, sum , created
Сделать миграцию, создающую таблицу archive с полями operation_id, sum, created, archived
Сделать миграцию, содержащую таблицу events с полями id, data (JSONB)
Сделать http API, которое принимает на вход число и создает запись в operation с соответствующей суммой.
После вставки в operations , срабатывает триггер в базе. Он вызывает хранимую процедуру, которая запаковывает все данные из новой строки в JSONB и заносит их в events.
Сделать демона, который непрерывно читает таблицу events и заносит ее данные в redis или rabbitMQ
Сделать демона , который читает redis или rabbitMQ и заносит данные в archive
```

## Конфигурация
В .env добавить
```bash
DAEMON_BACKEND=amphp
QUEUE_DSN=redis:tcp://127.0.0.1:6379
```
и указать параметры подключения к базе Postgres

## Установка
```bash
git clone git@bitbucket.org:Desperado/iq-options.git
cd iq-options
composer install -o -v
php artisan migrate:refresh --seed
```

## Запуск демона
```bash
php daemon.php
```

## Роут для добавления операции
```bash
/api/store/{number}
```
