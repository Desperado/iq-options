<?php

/**
 * Test task for IQ option company
 *
 * @author  Maksim Masiukevich <desperado@minsk-info.ru>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */

declare(strict_types = 1);

Route::put('/store/{number}', 'OperationController@store')
     ->where('number', '([0-9]*[.])?[0-9]+');